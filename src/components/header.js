import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"
import { FaBars } from "react-icons/fa"
import { menuData } from "../data/menuData"

export default function header() {
  return (
    <div>
      <Nav>
        <NavLink to="/">Bhaskar Dutta</NavLink>
        <Bars />
        <NavMenu>
          {menuData.map((item, index) => {
            return (
              <NavLink to={item.link} key={index} style={{ fontSize: "16px" }}>
                {item.title}
              </NavLink>
            )
          })}
        </NavMenu>
      </Nav>
    </div>
  )
}

const Nav = styled.nav`
  background: transparent;
  height: 80px;
  display: flex;
  justify-content: space-between;
  padding: 0.5rem calc((100vw - 1300px) / 2);
  z-index: 100;
  position: relative;
  /* width: 100vw; */
  /* background-color: red; */
`

const NavLink = styled(Link)`
  color: white;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;
  font-size: 1.5rem;
`

const Bars = styled(FaBars)`
  display: none;
  color: #fff;

  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`
const NavMenu = styled.div`
  display: flex;
  align-items: center;
  /* margin-right: -48px; */
  /* width: 100%; */
  /* width: 100vh; */
  @media screen and (max-width: 768px) {
    display: none;
  }
`

// const NavBtn = styled.div`
//   display: flex;
//   align-items: center;
//   margin-right: 24px;

//   @media screen and (max-width: 768px) {
//     display: none;
//   }
// `
