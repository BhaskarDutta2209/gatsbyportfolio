import React from "react"
import Video from "../assets/videos/coding.mp4"
import styled from "styled-components"

export default function Hero() {
  const message1 = <span style={{ color: "#eada61" }}>Hello World!</span>

  console.log(message1)

  return (
    <HeroContainer>
      <HeroBG>
        <VideoBG src={Video} type="video/mp4" autoPlay loop muted playsInline />
      </HeroBG>
      <HeroContent>
        <HeroItems>
          <HeroH1>
            <span style={{ color: "#29b5c5" }}>printf</span>
            <span style={{ color: "#d4d4d4" }}>(</span>
            <span style={{ color: "#eada61"}}>"Hello World!"</span>
            <span style={{ color: "#d4d4d4" }}>);</span>
          </HeroH1>
          <HeroP>Welcome to my Portfolio Website</HeroP>
        </HeroItems>
      </HeroContent>
    </HeroContainer>
  )
}

const HeroContainer = styled.div`
  background: linear-gradient(
    158.86deg,
    #1c2a34 10.36%,
    #111a20 49.58%,
    #111a20 49.58%
  );
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  padding: 0 1rem;
  position: relative;
  margin-top: -80px;
  color: #fff;

  :before {
    content: "";
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 2;
    background: linear-gradient(
        180deg,
        rgba(0, 0, 0, 0.2) 0%,
        rgba(0, 0, 0, 0.6) 100%
      ),
      linear-gradient(180deg, rgba(0, 0, 0, 0.2) 0%, transparent 100%);
  }
`

const HeroBG = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
`

const VideoBG = styled.video`
  width: 100%;
  height: 100%;
  -o-object-fit: cover;
  object-fit: cover;
  filter: opacity(95%) brightness(90%);
`

const HeroContent = styled.div`
  z-index: 4;
  height: calc(100vh - 80px);
  max-height: 100%;
  padding: 0rem calc((100vw - 1300px) / 2);
`

const HeroItems = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  height: 100vh;
  max-height: 100%;
  padding: 0;
  color: #fff;
  font-weight: bold;
  line-height: 1.1;
  font-size: 30px;
`

const HeroH1 = styled.h1`
  font-size: clamp(1.5rem, 6vw, 5rem);
  margin-bottom: 1.5rem;
  padding: 0 1rem;
`

const HeroP = styled.p`
  font-size: clamp(1rem, 3vw, 3rem);
  margin-bottom: 2rem;
`
