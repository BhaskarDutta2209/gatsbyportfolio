import React from "react"
import Illustration from "../assets/illustrations/Spaceship.png"
import styled from "styled-components"
import { Link } from "gatsby"
import {
  FaLinkedin,
  FaWhatsappSquare,
  FaGithubSquare,
  FaTelegram,
  FaMailBulk,
} from "react-icons/fa"
// import Aos from "aos"
// import "aos/dist/aos.css"

export default function About() {
  // Aos.init()
  return (
    // <ProductContainer>
    //   {/* <ProductHeading>Heading</ProductHeading>
    //   <ProductWrapper>Wrapper</ProductWrapper> */}
    //   <AboutIllustration src={Illustration}/>
    //   <AboutDetails>Voluptate quis magna ex excepteur ex esse tempor magna aliquip deserunt nostrud. In esse officia esse magna fugiat magna nulla occaecat amet ipsum et sunt nisi. Labore ullamco non mollit Lorem dolor voluptate enim eu proident.</AboutDetails>
    // </ProductContainer>
    <AboutContainer>
      {/* <AboutHeading>
        <span style={{color: "white"}}>About</span> <span>Me</span>
      </AboutHeading> */}
      <AboutIllustrationAndDetails>
        <AboutIllustration src={Illustration} />
        <AboutDetails data-aos="fade-right" data-aos-duration="1500">
          <AboutInOneLine>
            I'm Bhaskar, a software developer and researcher
          </AboutInOneLine>
          <AboutInPara>
            I specialize in blockchain development. I talk about my
            work on{" "}
            <Link
              to="https://www.linkedin.com/in/itsbhaskardutta/"
              style={{ textDecoration: "none", color: "#1DA1F2" }}
            >
              LinkedIn
            </Link>
            , commit code to{" "}
            <Link
              to="https://github.com/BhaskarDutta2209"
              style={{ textDecoration: "none", color: "#BBC6CC" }}
            >
              Github
            </Link>{" "}
            and chat on{" "}
            <Link
              to="https://wa.me/917439146638"
              style={{ textDecoration: "none", color: "#EA4C89" }}
            >
              Whatsapp
            </Link>
            .{" "}
          </AboutInPara>
          <AboutIcons>
            <LinkedIcon to="https://www.linkedin.com/in/itsbhaskardutta/">
              <FaLinkedin style={{ fontSize: "2rem", color: "#1DA1F2" }} />
            </LinkedIcon>
            <LinkedIcon to="https://github.com/BhaskarDutta2209">
              <FaGithubSquare
                style={{ fontSize: "2rem", color: "#BBC6CC" }}
              />
            </LinkedIcon>
            <LinkedIcon to="https://wa.me/917439146638">
              <FaWhatsappSquare
                style={{ fontSize: "2rem", color: "#25D366" }}
              />
            </LinkedIcon>
            <LinkedIcon to="https://telegram.me/itsmebd">
              <FaTelegram style={{ fontSize: "2rem", color: "#0088CC" }} />
            </LinkedIcon>
          </AboutIcons>
        </AboutDetails>
      </AboutIllustrationAndDetails>
    </AboutContainer>
  )
}

const AboutContainer = styled.div`
  min-height: 50vh;
  padding: 1rem;
  background: #050405;
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

// const AboutHeading = styled.h1`
//   color: #83f9a2;
//   font-size: 2.5rem;
//   font-weight: 900;
//   text-decoration: solid;
// `
const AboutIllustrationAndDetails = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 90vw;
`
const AboutIllustration = styled.img`
  width: 40vw;
  height: 40vw;
  /* background-color: red; */
  object-fit: cover;
`
const AboutDetails = styled.div`
  height: 50vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-right: 2rem;
`
const AboutInOneLine = styled.h4`
  font-size: clamp(1rem, 3.5vw, 4rem);
  color: #83f9a2;
  padding-bottom: 1.5rem;
`

const AboutInPara = styled.p`
  font-size: clamp(0.5rem, 1.75vw, 2rem);
`

const AboutIcons = styled.div`
  display: flex;
  justify-self: center;
  justify-content: center;
  margin-top: 20px;
  /* background-color: red; */
`

const LinkedIcon = styled(Link)`
  margin: 10px;
`
