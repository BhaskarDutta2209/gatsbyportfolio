import React from 'react'
import Header from "./header"
import { GlobalStyle } from './styles/globalStyles'

export default function layout({children}) {
  return (
    // <div style={{background: "linear-gradient(158.86deg, #1C2A34 10.36%, #111A20 49.58%, #111A20 49.58%)", color: "white"}}>
    <div style={{background: "black", color: "white"}}>
      <GlobalStyle />
      <Header />
      {children}
    </div>
  )
}
