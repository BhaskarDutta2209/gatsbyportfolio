export const menuData = [
  {title: "About", link: "/about"},
  {title: "Researcher", link: "/researcher"},
  {title: "Developer", link: "/developer"},
  {title: "Contact", link: "/contact"},
]