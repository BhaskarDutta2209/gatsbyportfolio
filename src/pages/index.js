import * as React from "react"

import Layout from "../components/layout"
import Hero from "../components/hero"
import About from "../components/about"
import Seo from "../components/seo"

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <Hero />
    <About />
  </Layout>
)

export default IndexPage
