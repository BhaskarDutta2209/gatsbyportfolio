import * as React from "react"
import styled from "styled-components"
import { GlobalStyle } from "../components/styles/globalStyles"
import Astronaut from "../assets/illustrations/confusedAstronaut.png"
import Space from "../assets/illustrations/blankspace.png"

// import Layout from "../components/layout"
// import Seo from "../components/seo"

export default function NotFoundPage() {
  return (
    <div style={{height: "90vh" }}>
      <GlobalStyle />
      <SpaceBackground src={Space}></SpaceBackground>;
      <div
        style={{
          display: "flex",
          alignContent: "center",
          justifyContent: "center",
          alignSelf: "center",
          height: "100%",
          width: "100%",
        }}
      >
        <ConfusedAstronaut src={Astronaut} />
      </div>
    </div>
  )
}

const ConfusedAstronaut = styled.img`
  position: absolute;
  /* top: 40vh; */
  height: 60%;
  align-self: center;
  justify-self: center;
`
const SpaceBackground = styled.img`
  position: absolute;
  height: 100%;
  width: 100%;
`